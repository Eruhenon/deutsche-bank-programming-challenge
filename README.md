# Deutsche Bank Programming Challenge
This is my solution for the programming Challenge.

## Software Requirements
- Xcode: 10.3
- Swift: Apple Swift version 5.0.1
- Carthage: 0.33.0
- SwiftGen: SwiftGen v6.1.0 (Stencil v0.13.1, StencilSwiftKit v2.7.2, SwiftGenKit v6.1.0)

## Install
run bootstrap.sh within the projects root directory
