//
//  HTTPClientTests.swift
//  DB Programming ChallengeTests
//
//  Created by Jan Olbrich on 13.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

import Quick
import Nimble
import OHHTTPStubs

@testable import DB_Programming_Challenge

class HTTPClientTests: QuickSpec {
    override func spec() {
        describe("The HTTPClient") {
            context("requesting existing data via GET") {
                beforeEach {
                    stub(condition: isHost("testtarget.com") && isPath("/data/existing")) { _ in
                        let obj: [String: Any] = ["name":"John Doe", "age":55]
                        return OHHTTPStubsResponse(jsonObject: obj, statusCode: 200, headers: nil)
                    }
                }
                
                afterEach {
                    OHHTTPStubs.removeAllStubs()
                }
                
                it("returns the correct data") {
                    let sut: HTTPClient = HTTPClientImpl()
                    let url = URL(string: "http://testtarget.com/data/existing")!
                    var result: HTTPDemoJSON?
                    sut.decodedJsonGet(url: url, type: HTTPDemoJSON.self) { (data, error) in
                        result = data
                    }
                    
                    expect(result).toEventuallyNot(beNil())
                }
                
                it("parses the object correctly") {
                    let sut: HTTPClient = HTTPClientImpl()
                    let url = URL(string: "http://testtarget.com/data/existing")!
                    var result: HTTPDemoJSON?
                    sut.decodedJsonGet(url: url, type: HTTPDemoJSON.self) { (data, error) in
                        result = data
                    }
                    
                    expect(result?.name).toEventually(equal("John Doe"))
                    expect(result?.age).toEventually(equal(55))
                }
            }
            
            context("requesting nonexisting data via GET") {
                beforeEach {
                    stub(condition: isHost("testtarget.com") && isPath("/data/missing")) { _ in
                        let missingDataError = NSError(domain: NSURLErrorDomain, code: -100)
                        return OHHTTPStubsResponse(error:missingDataError)
                    }
                }
                
                afterEach {
                    OHHTTPStubs.removeAllStubs()
                }
                
                it("returns the correct data") {
                    let sut: HTTPClient = HTTPClientImpl()
                    let url = URL(string: "http://testtarget.com/data/missing")!
                    var result: Error?
                    sut.decodedJsonGet(url: url, type: HTTPDemoJSON.self) { (data, error) in
                        result = error
                    }
                    
                    expect(result).toEventuallyNot(beNil())
                }
            }
        }
    }
}
