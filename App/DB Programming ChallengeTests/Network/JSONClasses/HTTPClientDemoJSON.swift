//
//  HTTPClientDemoJSON.swift
//  DB Programming ChallengeTests
//
//  Created by Jan Olbrich on 13.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

import Foundation

struct HTTPDemoJSON: Decodable {
    let name: String
    let age: Int
}
