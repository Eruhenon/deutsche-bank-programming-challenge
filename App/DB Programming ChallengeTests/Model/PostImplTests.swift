//
//  PostImplTests.swift
//  DB Programming ChallengeTests
//
//  Created by Jan Olbrich on 14.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

import Quick
import Nimble
import CoreData

@testable import DB_Programming_Challenge

class PostImplTests: QuickSpec {
    override func spec() {
        describe("The PostImpl") {
            let postID = 1
            let userID = 2
            let title = "TestTitle"
            let body = "TestBody"
            
            context("initialized with a JSONPost") {
                let jsonPost = JSONPost(id: postID, userId: userID, title: title, body: body)
                
                it("has the correct ID") {
                    let sut = PostImpl(json: jsonPost)
                    expect(sut.id).to(equal(postID))
                }
                
                it("has the correct userID") {
                    let sut = PostImpl(json: jsonPost)
                    expect(sut.userId).to(equal(userID))
                }
                
                it("has the correct title") {
                    let sut = PostImpl(json: jsonPost)
                    expect(sut.title).to(equal(title))
                }
                
                it("has the correct body") {
                    let sut = PostImpl(json: jsonPost)
                    expect(sut.body).to(equal(body))
                }
                
                it("has the correct favorite state") {
                    let sut = PostImpl(json: jsonPost)
                    expect(sut.favorite).to(beFalse())
                }
            }
            
            context("initializing with a CoreData entity") {
                var coreDataManager: CoreDataManagerImpl!
                var sut: PlaceholderPost!
                
                beforeEach {
                    coreDataManager = CoreDataManagerImpl()
                    coreDataManager.setup(type: NSInMemoryStoreType) {}
                    let context = coreDataManager.backgroundContext
                    waitUntil { done in
                        context.performAndWait {
                            sut = NSEntityDescription.insertNewObject(forEntityName: PlaceholderPost.entityName(), into: context) as? PlaceholderPost
                            
                            sut.id = Int64(postID)
                            sut.userId = Int64(userID)
                            sut.title = title
                            sut.body = body
                            
                            try! context.save()
                            try! coreDataManager.mainContext.save()
                        }
                        done()
                    }
                }
                
                afterEach {
                    sut = nil
                    coreDataManager = nil
                }
                
                it("has the correct ID") {
                    let sut = PostImpl(coreData: sut)
                    expect(sut.id).toEventually(equal(postID))
                }
                
                it("has the correct userId") {
                    let sut = PostImpl(coreData: sut)
                    expect(sut.userId).toEventually(equal(userID))
                }
                
                it("has the correct title") {
                    let sut = PostImpl(coreData: sut)
                    expect(sut.title).toEventually(equal(title))
                }
                
                it("has the correct body") {
                    let sut = PostImpl(coreData: sut)
                    expect(sut.body).toEventually(equal(body))
                }
            }
        }
    }
}
