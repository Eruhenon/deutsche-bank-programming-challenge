//
//  CoreDataDatabaseTests.swift
//  DB Programming ChallengeTests
//
//  Created by Jan Olbrich on 14.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

import Quick
import Nimble
import CoreData

@testable import DB_Programming_Challenge

class CoreDataDatabaseTests: QuickSpec {
    override func spec() {
        describe("The CoreDataDatabase") {
            context("adding values") {
                var result: [Post]!
                
                beforeEach {
                    let coreDataManager = CoreDataManagerImpl()
                    coreDataManager.setup(type: NSInMemoryStoreType) {}
                    let sut = CoreDataDatabase(manager: coreDataManager)
                    
                    let demoData = [PostImpl(testID: 50, testUserId: 123, testTitle: "demoTitle", testBody: "demoCoreDataBody", testLike: true), PostImpl(), PostImpl()]
                    sut.update(posts: demoData)
                    
                    result = sut.posts(for: UserImpl(userID: 123))
                }
                
                afterEach {
                    result = nil
                }
                
                it("contains the same amount of values added") {
                    expect(result.count).to(equal(1))
                }
                
                it("contains the correct id for the first post") {
                    expect(result.first?.id).to(equal(50))
                }
                
                it("contains the correct userId for the first post") {
                    expect(result.first?.userId).to(equal(123))
                }
                
                it("contains the correct title for the first post") {
                    expect(result.first?.title).to(equal("demoTitle"))
                }
                
                it("contains the correct body for the first post") {
                    expect(result.first?.body).to(equal("demoCoreDataBody"))
                }
                
                it("contains the correct favorite for the first post") {
                    expect(result.first?.favorite).to(beTrue())
                }
            }
            
            context("requesting an existing post") {
                it("returns the correct post") {
                    let coreDataManager = CoreDataManagerImpl()
                    coreDataManager.setup(type: NSInMemoryStoreType) {}
                    let sut = CoreDataDatabase(manager: coreDataManager)
                    
                    let demoData = [PostImpl(testID: 50, testUserId: 123, testTitle: "demoPostTitle", testBody: "demoCoreDataBody"), PostImpl(), PostImpl()]
                    sut.update(posts: demoData)
                    
                    let post = sut.post(id: 50)
                    
                    expect(post?.title).to(equal("demoPostTitle"))
                }
            }
            
            context("requesting a non existing post") {
                it("returns nil") {
                    let coreDataManager = CoreDataManagerImpl()
                    coreDataManager.setup(type: NSInMemoryStoreType) {}
                    let sut = CoreDataDatabase(manager: coreDataManager)
                    
                    let demoData = [PostImpl(testID: 50, testUserId: 123, testTitle: "demoPostTitle", testBody: "demoCoreDataBody"), PostImpl(), PostImpl()]
                    sut.update(posts: demoData)
                    
                    var post: Post? = PostImpl()
                    post = sut.post(id: 500)
                    
                    expect(post).to(beNil())
                }
            }
            
            context("updating a post") {
                it("changes the values accordingly") {
                    let coreDataManager = CoreDataManagerImpl()
                    coreDataManager.setup(type: NSInMemoryStoreType) {}
                    let sut = CoreDataDatabase(manager: coreDataManager)
                    
                    let demoData = [PostImpl(testID: 50, testUserId: 123, testTitle: "demoPostTitle", testBody: "demoCoreDataBody"), PostImpl(), PostImpl()]
                    sut.update(posts: demoData)
                    let post = sut.post(id: 50)!
                    
                    let newPost = PostImpl(post: post, newFavorite: !post.favorite)
                    sut.update(post: newPost)
                    
                    let result = sut.post(id: 50)
                    
                    expect(result?.favorite).to(equal(newPost.favorite))
                }
            }
            
            context("deleting posts of a user") {
                it("returns the updated post count") {
                    let coreDataManager = CoreDataManagerImpl()
                    coreDataManager.setup(type: NSInMemoryStoreType) {}
                    let sut = CoreDataDatabase(manager: coreDataManager)
                    
                    let demoData = [PostImpl(testID: 50, testUserId: 123, testTitle: "demoPostTitle", testBody: "demoCoreDataBody"), PostImpl(), PostImpl()]
                    sut.update(posts: demoData)
                    
                    sut.deletePosts(for: UserImpl(userID: 123))
                    
                    let result = sut.posts(for: UserImpl(userID: 123))
                    
                    expect(result.count).to(equal(0))
                }
            }
        }
    }
}
