//
//  CoreDataManagerTests.swift
//  DB Programming ChallengeTests
//
//  Created by Jan Olbrich on 14.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

import Quick
import Nimble
import CoreData

@testable import DB_Programming_Challenge

class CoreDataManagerTests: QuickSpec {
    override func spec() {
        describe("The CoreDataManager") {
            context("setting up the store") {
                it("calls setup complete") {
                    let sut = CoreDataManagerImpl()
                    
                    var result = false
                    sut.setup(type: NSInMemoryStoreType) {
                        result = true
                    }
                    
                    expect(result).toEventually(beTrue())
                }
                
                it("has stores setup") {
                    let sut = CoreDataManagerImpl()
                    
                    sut.setup(type: NSInMemoryStoreType) { }
                    
                    expect(sut.persistentContainer.persistentStoreCoordinator.persistentStores.count).toEventually(beGreaterThan(0))
                }
                
                it("creates background Contexts") {
                    let sut = CoreDataManagerImpl()
                    
                    var didCreateBackgroundContextCorrectly = false
                    sut.setup(type: NSInMemoryStoreType) {
                        didCreateBackgroundContextCorrectly = sut.backgroundContext.concurrencyType == .privateQueueConcurrencyType
                    }
                    
                    expect(didCreateBackgroundContextCorrectly).toEventually(beTrue())
                }
                
                it("creates main Contexts") {
                    let sut = CoreDataManagerImpl()
                    
                    var didCreateMainContextCorrectly = false
                    sut.setup(type: NSInMemoryStoreType) {
                        didCreateMainContextCorrectly = sut.mainContext.concurrencyType == .mainQueueConcurrencyType
                    }
                    
                    expect(didCreateMainContextCorrectly).toEventually(beTrue())
                }
                
                context("passing the storetype") {
                    it("creates an inmemory store") {
                        let sut = CoreDataManagerImpl()
                        
                        sut.setup(type: NSInMemoryStoreType) { }
                        
                        expect(sut.persistentContainer.persistentStoreDescriptions.first?.type).toEventually(equal(NSInMemoryStoreType))
                    }
                    
                    it("creates a store on disk") {
                        let sut = CoreDataManagerImpl()
                        
                        sut.setup() { }
                        
                        expect(sut.persistentContainer.persistentStoreDescriptions.first?.type).toEventually(equal(NSSQLiteStoreType))
                        sut.persistentContainer.destroyPersistentStore()
                    }
                }
            }
        }
    }
}
