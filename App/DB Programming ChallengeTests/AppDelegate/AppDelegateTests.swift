//
//  AppDelegateTests.swift
//  DB Programming ChallengeTests
//
//  Created by Jan Olbrich on 14.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

import Quick
import Nimble

@testable import DB_Programming_Challenge

class AppDelegateTests: QuickSpec {
    override func spec() {
        describe("The AppDelegate") {
            context("after calling didFinishLaunchingWithOptions") {
                var sut: AppDelegate!
                beforeEach {
                    sut = AppDelegate()
                    _ = sut.application(UIApplication.shared, didFinishLaunchingWithOptions: nil)
                }
                
                afterEach {
                    sut.window?.resignKey()
                    sut.window?.isHidden = true
                    sut = nil
                }
                
                it("has a non nil window") {
                    expect(sut.window).notTo(beNil())
                }
                
                it("has a visible and key window") {
                    expect(sut.window?.isKeyWindow).to(beTrue())
                    expect(sut.window?.isHidden).notTo(beTrue())
                }
                
                it("has the LoginViewController as RootViewController") {
                    let vc = sut.window?.rootViewController as! UINavigationController
                    expect(vc.topViewController).to(beAKindOf(LoginViewController.self))
                }
            
                it("has set the viewModel in LoginViewController") {
                    let vc = sut.window?.rootViewController as! UINavigationController
                    let loginVC = vc.topViewController as! LoginViewController
                    
                    expect(loginVC.viewModel).notTo(beNil())
                }
                
                it("has set the coordinator in LoginViewController") {
                    let vc = sut.window?.rootViewController as! UINavigationController
                    let loginVC = vc.topViewController as! LoginViewController
                    
                    expect(loginVC.coordinator).notTo(beNil())
                }
            }
        }
    }
}
