//
//  LoginViewModelMock.swift
//  DB Programming ChallengeTests
//
//  Created by Jan Olbrich on 13.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

@testable import DB_Programming_Challenge

class LoginViewModelMock: LoginViewModel {
    let userIDLabelText = "Mock text for the userIDLabelText"
    let loginButtonTitle = "Mock text for the loginButtonTitle"
    
    var delegate: LoginViewModelDelegate?
    var segueData: StoryboardStageData? {
        guard let userID = Int(providedID) else {
            return nil
        }
        return LoginStageData(user: UserImpl(userID: userID))
    }
    
    var didCallLogin = false
    var providedID = ""
    
    func login(with userID: String) {
        didCallLogin = true
        providedID = userID
    }
}
