//
//  StoryboardStageVisitorMock.swift
//  DB Programming ChallengeTests
//
//  Created by Jan Olbrich on 13.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

import UIKit

@testable import DB_Programming_Challenge

class StoryboardStageVisitorMock: StoryboardStageVisitor {
    var httpClient: HTTPClient = HTTPClientMock(data: [], error: nil)
    var coreDataManager: CoreDataManager = CoreDataManagerImpl()
    var didCallPrepare = false
    var receivedUser: User?
    
    func prepare(for segue: UIStoryboardSegue, sender: Any?, context: StoryboardStageData?) {
        didCallPrepare = true
        
        if let stageData = context as? LoginStageData {
            receivedUser = stageData.user
        }
    }
}
