//
//  LoginViewModelDelegateMock.swift
//  DB Programming ChallengeTests
//
//  Created by Jan Olbrich on 13.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

@testable import DB_Programming_Challenge

class LoginViewModelDelegateMock: LoginViewModelDelegate {
    var didCallLoginWithUser = false
    var didCallLoginFailed = false
    var user: User?
    
    func loginViewModel(_: LoginViewModel, didLoginWith user: User) {
        didCallLoginWithUser = true
        self.user = user
    }
    
    func loginViewModel(_: LoginViewModel, loginDidFailWithError: Error) {
        didCallLoginFailed = true
    }
}
