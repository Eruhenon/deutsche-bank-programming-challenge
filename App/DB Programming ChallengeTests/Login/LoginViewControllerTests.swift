//
//  LoginViewControllerTests.swift
//  DB Programming ChallengeTests
//
//  Created by Jan Olbrich on 13.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

import Quick
import Nimble

@testable import DB_Programming_Challenge

class LoginViewControllerTests: QuickSpec {
    override func spec() {
        describe("The LoginViewController") {
            var sut: LoginViewController!
            
            context("after initialization") {
                beforeEach {
                    sut = StoryboardScene.ViewPosts.loginViewScene.instantiate()
                    sut.viewModel = LoginViewModelMock()
                    sut.coordinator = StoryboardStageVisitorMock()
                    sut.initializeLifeCycle()
                }
                
                afterEach {
                    sut = nil
                }
                
                it("is non nil") {
                    expect(sut).notTo(beNil())
                }
                
                it("has an outlet to the idLabel") {
                    expect(sut.userIDLabel).notTo(beNil())
                }
                
                it("has an outlet to the idTextField") {
                    expect(sut.userIDTextField).notTo(beNil())
                }
                
                it("has an outlet to the loginButton") {
                    expect(sut.loginButton).notTo(beNil())
                }
            }
            
            context("the loginButton") {
                beforeEach {
                    sut = StoryboardScene.ViewPosts.loginViewScene.instantiate()
                    sut.viewModel = LoginViewModelMock()
                    sut.coordinator = StoryboardStageVisitorMock()
                    sut.initializeLifeCycle()
                }
                
                afterEach {
                    sut = nil
                }
                
                it("has an action on EventTouchUpInside") {
                    expect(sut.loginButton.actions(forTarget: sut, forControlEvent: .touchUpInside)?.count).to(beGreaterThan(0))
                }
            }
            
            context("setting the ViewModel and calling ViewWillAppear") {
                var mock: LoginViewModelMock!
                
                beforeEach {
                    sut = StoryboardScene.ViewPosts.loginViewScene.instantiate()
                    sut.coordinator = StoryboardStageVisitorMock()
                    
                    mock = LoginViewModelMock()
                    sut.viewModel = mock
                    
                    sut.initializeLifeCycle()
                    sut.viewWillAppear(false)
                }
                
                afterEach {
                    mock = nil
                    sut = nil
                }
                
                it("sets itself as the ViewModelDelegate") {
                    expect(sut.viewModel.delegate).to(be(sut))
                }
                
                it("sets the userIDLabel text") {
                    expect(sut.userIDLabel.text).to(equal(mock.userIDLabelText))
                }
                
                it("sets the loginButton title") {
                    expect(sut.loginButton.title(for:.normal)).to(equal(mock.loginButtonTitle))
                }
            }
            
            context("pressing login") {
                var mock: LoginViewModelMock!
                
                beforeEach {
                    sut = StoryboardScene.ViewPosts.loginViewScene.instantiate()
                    sut.coordinator = StoryboardStageVisitorMock()
                    
                    mock = LoginViewModelMock()
                    sut.viewModel = mock
                    
                    sut.initializeLifeCycle()
                    sut.viewWillAppear(false)
                }
                
                afterEach {
                    mock = nil
                    sut = nil
                }
                
                it("calls login on the ViewModel") {
                    sut.loginButton.sendActions(for: .touchUpInside)
                    expect(mock.didCallLogin).to(beTrue())
                }
                
                it("sends the textField Value to the ViewModel") {
                    let testUserID = "123456789"
                    sut.userIDTextField.text = testUserID
                    sut.loginButton.sendActions(for: .touchUpInside)
                    expect(mock.providedID).to(equal(testUserID))
                }
                
                context("on successful login") {
                    it("executes it's segue") {
                        sut.loginViewModel(sut.viewModel, didLoginWith: UserImpl(userID: 123))
                        
                        let coordinator = sut.coordinator as! StoryboardStageVisitorMock
                        expect(coordinator.didCallPrepare).toEventually(beTrue())
                    }
                }
            }
            
            context("presenting the next VC") {
                it("calls it's coordinator") {
                    let mock = StoryboardStageVisitorMock()
                    sut = StoryboardScene.ViewPosts.loginViewScene.instantiate()
                    sut.viewModel = LoginViewModelMock()
                    sut.coordinator = mock
                    
                    let segue = UIStoryboardSegue(identifier: "testSegue", source: sut, destination: sut)
                    sut.prepare(for: segue, sender: nil)
                    
                    expect(mock.didCallPrepare).toEventually(beTrue())
                }
                
                it("passes the user from the viewModel to the coordinator") {
                    let mock = StoryboardStageVisitorMock()
                    sut = StoryboardScene.ViewPosts.loginViewScene.instantiate()
                    
                    let loginViewModelMock = LoginViewModelMock()
                    loginViewModelMock.login(with: "1234")
                    sut.viewModel = loginViewModelMock
                    sut.coordinator = mock
                    
                    let segue = UIStoryboardSegue(identifier: "testSegue", source: sut, destination: sut)
                    sut.prepare(for: segue, sender: nil)
                    
                    expect(mock.receivedUser?.userID).to(equal(1234))
                }
            }
        }
    }
}
