//
//  LoginViewModelTests.swift
//  DB Programming ChallengeTests
//
//  Created by Jan Olbrich on 13.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

import Quick
import Nimble

@testable import DB_Programming_Challenge

class LoginViewModelTests: QuickSpec {
    override func spec() {
        describe("The Login ViewModel") {
            context("after initialization") {
                it("provides the userIDLabelText") {
                    let sut: LoginViewModel = LoginViewModelImpl()
                    expect(sut.userIDLabelText).to(equal("UserID:"))
                }
                
                it("provides the loginButtonTitle") {
                    let sut: LoginViewModel = LoginViewModelImpl()
                    expect(sut.loginButtonTitle).to(equal("Login"))
                }
            }
            
            context("calling login(with:) with a valid ID") {
                it("calls its delegate successfully") {
                    let sut: LoginViewModel = LoginViewModelImpl()
                    let mock = LoginViewModelDelegateMock()
                    sut.delegate = mock
                    sut.login(with: "123456789")
                    
                    expect(mock.didCallLoginWithUser).toEventually(beTrue())
                }
                
                it("provides the corresponding user") {
                    let sut: LoginViewModel = LoginViewModelImpl()
                    let mock = LoginViewModelDelegateMock()
                    let userID = "123456789"
                    sut.delegate = mock
                    sut.login(with: userID)
                    
                    expect(mock.user?.userID).toEventually(equal(Int(userID)))
                }
            }
            
            context("calling login(with:) with an invalid ID") {
                it("calls its delegate failure") {
                    let sut: LoginViewModel = LoginViewModelImpl()
                    let mock = LoginViewModelDelegateMock()
                    sut.delegate = mock
                    sut.login(with: "a12bc")
                    
                    expect(mock.didCallLoginFailed).toEventually(beTrue())
                }
            }
        }
    }
}
