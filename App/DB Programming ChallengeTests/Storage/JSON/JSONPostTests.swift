//
//  JSONPostTests.swift
//  DB Programming ChallengeTests
//
//  Created by Jan Olbrich on 14.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

import Quick
import Nimble

@testable import DB_Programming_Challenge

class JSONPostTests: QuickSpec {
    override func spec() {
        let title = "sunt aut facere repellat provident occaecati excepturi optio reprehenderit"
        let body = "quia et suscipit suscipit recusandae consequuntur expedita et cum reprehenderit molestiae ut ut quas totam nostrum rerum est autem sunt rem eveniet architecto"
        let demoData = """
            {
            "userId": 1,
            "id": 1,
            "title": "\(title)",
            "body": "\(body)"
            }
            """.data(using: .utf8)!
        describe("The JSONPost") {
            context("decoded from a valid JSON") {
                it("contains the correct ID") {
                    let result = try! JSONDecoder().decode(JSONPost.self, from: demoData)
                    expect(result.id).to(equal(1))
                }
                
                it("contains the correct userID") {
                    let result = try! JSONDecoder().decode(JSONPost.self, from: demoData)
                    expect(result.userId).to(equal(1))
                }
                
                it("contains the correct title") {
                    let result = try! JSONDecoder().decode(JSONPost.self, from: demoData)
                    expect(result.title).to(equal(title))
                }
                
                it("contains the correct body") {
                    let result = try! JSONDecoder().decode(JSONPost.self, from: demoData)
                    expect(result.body).to(equal(body))
                }
            }
        }
    }
}
