//
//  PostsStorageTests.swift
//  DB Programming ChallengeTests
//
//  Created by Jan Olbrich on 13.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

import Quick
import Nimble

@testable import DB_Programming_Challenge

class PostsStorageTests: QuickSpec {
    override func spec() {
        describe("The PostsStorage") {
            context("calling update") {
                it("requests data from the correct URL") {
                    let mock = HTTPClientMock()
                    let sut: PostsStorage = PostsStorageImpl(httpClient: mock, database: DatabaseMock())
                    
                    sut.update(for: UserImpl(userID: 123))
                    
                    let targetURL = URL(string: "https://jsonplaceholder.typicode.com/posts?userId=123")!
                    expect(mock.didUpdateURL).toEventually(equal(targetURL))
                }
                
                it("informs the delegate about new data") {
                    let sut: PostsStorage = PostsStorageImpl(httpClient: HTTPClientMock(data: [JSONPost(), JSONPost()]), database: DatabaseMock())
                    let mock = PostsStorageDelegateMock()
                    sut.delegate = mock
                    
                    sut.update(for: UserImpl(userID: 123))
                    
                    expect(mock.didReceiveUpdate).toEventually(beTrue())
                }
                
                context("receiving Data") {
                    it("passes all the data") {
                        let sut: PostsStorage = PostsStorageImpl(httpClient: HTTPClientMock(data: [JSONPost(), JSONPost()]), database: DatabaseMock())
                        let mock = PostsStorageDelegateMock()
                        sut.delegate = mock
                        
                        sut.update(for: UserImpl(userID: 123))
                        
                        expect(mock.dataReceived?.count).toEventually(equal(2))
                    }
                    
                    it("stores the posts") {
                        let mock = DatabaseMock()
                        let sut: PostsStorage = PostsStorageImpl(httpClient: HTTPClientMock(data: [JSONPost(), JSONPost()]), database: mock)
                        sut.delegate = PostsStorageDelegateMock()
                        
                        sut.update(for: UserImpl(userID: 123))
                        
                        expect(mock.data.count).toEventually(equal(2))
                    }
                    
                    it("preserves the favorite state") {
                        let mock = DatabaseMock()
                        let sut: PostsStorage = PostsStorageImpl(httpClient: HTTPClientMock(data: [JSONPost(testUserId: 123)]), database: mock)
                        sut.delegate = PostsStorageDelegateMock()
                        
                        sut.update(for: UserImpl(userID: 123))
                        
                        mock.data[0] = PostImpl(post: mock.data[0], newFavorite: true)
                        
                        sut.update(for: UserImpl(userID: 123))
                        
                        expect(mock.data[0].favorite).toEventually(beTrue())
                    }
                    
                    it("doesn't create duplicates") {
                        let mock = DatabaseMock()
                        let sut: PostsStorage = PostsStorageImpl(httpClient: HTTPClientMock(data: [JSONPost(testUserId: 123), JSONPost(testUserId: 123)]), database: mock)
                        sut.delegate = PostsStorageDelegateMock()
                        
                        sut.update(for: UserImpl(userID: 123))
                        sut.update(for: UserImpl(userID: 123))
                        
                        expect(mock.data.count).toEventually(equal(2))
                    }
                }
                
                context("receiving an error") {
                    it("invoked the error delegate method") {
                        let sut: PostsStorage = PostsStorageImpl(httpClient: HTTPClientMock(error: NSError(domain: "TestError", code: -1, userInfo: nil)), database: DatabaseMock())
                        let mock = PostsStorageDelegateMock()
                        sut.delegate = mock
                        
                        sut.update(for: UserImpl(userID: 123))
                        
                        expect(mock.didReceiveError).toEventually(beTrue())
                    }
                }
            }
            
            context("requesting posts") {
                it("returns all posts for the user within the database") {
                    let mock = DatabaseMock(posts: [PostImpl(testID: 100), PostImpl(), PostImpl()])
                    let sut: PostsStorage = PostsStorageImpl(httpClient: HTTPClientMock(), database: mock)
                    
                    let posts = sut.posts(for: UserImpl(userID: 123))
                    
                    expect(posts.count).to(equal(3))
                }
            }
            
            context("requesting to change a favorite") {
                it("creates a change request to the DB") {
                    let mock = DatabaseMock(posts: [PostImpl(testID: 1), PostImpl(testID: 2), PostImpl(testID: 3)])
                    let sut: PostsStorage = PostsStorageImpl(httpClient: HTTPClientMock(), database: mock)
                    
                    sut.toggleFavorite(post: 1)
                    
                    expect(mock.didRequestChange).to(beTrue())
                }
                
                it("creates a change request to the DB") {
                    let mock = DatabaseMock(posts: [PostImpl(testID: 1), PostImpl(testID: 2), PostImpl(testID: 3)])
                    let sut: PostsStorage = PostsStorageImpl(httpClient: HTTPClientMock(), database: mock)
                    
                    sut.toggleFavorite(post: 1)
                    
                    expect(mock.didRequestChange).to(beTrue())
                }
                
                it("informs its delegate about updates") {
                    let dbMock = DatabaseMock(posts: [PostImpl(testID: 1), PostImpl(testID: 2), PostImpl(testID: 3)])
                    let sut: PostsStorage = PostsStorageImpl(httpClient: HTTPClientMock(), database: dbMock)
                    let mock = PostsStorageDelegateMock()
                    sut.delegate = mock
                    
                    sut.toggleFavorite(post: 1)
                    
                    expect(mock.didReceiveUpdate).toEventually(beTrue())
                }
                
                it("provides the changed post") {
                    let mock = DatabaseMock(posts: [PostImpl(testID: 1), PostImpl(testID: 2), PostImpl(testID: 3)])
                    let sut: PostsStorage = PostsStorageImpl(httpClient: HTTPClientMock(), database: mock)
                    
                    sut.toggleFavorite(post: 1)
                    
                    expect(mock.changePost?.favorite).to(beTrue())
                }
            }
        }
    }
}
