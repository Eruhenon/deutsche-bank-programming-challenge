//
//  HTTPClientMock.swift
//  DB Programming ChallengeTests
//
//  Created by Jan Olbrich on 13.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

import Foundation

@testable import DB_Programming_Challenge

class HTTPClientMock: HTTPClient {
    var didUpdateURL: URL?
    
    private var responseData: [JSONPost]?
    private var responseError: Error?
    
    init(data: [JSONPost]? = nil, error: Error? = nil) {
        responseData = data
        responseError = error
    }
    
    func decodedJsonGet<T>(url: URL, type: T.Type, completion: @escaping (T?, Error?) -> Void) where T : Decodable {
        didUpdateURL = url
        
        completion(responseData as? T, responseError)
    }
}
