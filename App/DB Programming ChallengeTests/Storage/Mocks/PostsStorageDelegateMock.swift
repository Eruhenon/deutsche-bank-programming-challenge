//
//  PostsStorageDelegateMock.swift
//  DB Programming ChallengeTests
//
//  Created by Jan Olbrich on 13.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

@testable import DB_Programming_Challenge

class PostsStorageDelegateMock: PostsStorageDelegate {
    var didReceiveUpdate = false
    var didReceiveError = false
    var dataReceived: [Post]?
    
    func postsStorage(_ storage: PostsStorage, didUpdate posts: [Post]) {
        didReceiveUpdate = true
        dataReceived = posts
    }
    
    func postsStorage(_ storage: PostsStorage, didFailToUpdate: Error) {
        didReceiveError = true
    }
}
