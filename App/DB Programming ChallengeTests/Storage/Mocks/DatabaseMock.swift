//
//  DatabaseMock.swift
//  DB Programming ChallengeTests
//
//  Created by Jan Olbrich on 13.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

@testable import DB_Programming_Challenge

class DatabaseMock: Database {
    var data: [Post] = []
    
    var didRequestChange = false
    var changePost: Post?
    
    init(posts: [Post] = []) {
        data = posts
    }
    
    func posts(for user: User) -> [Post] {
        return data
    }
    
    func update(posts: [Post]) {
        data += posts
    }
    
    func update(post: Post) {
        didRequestChange = true
        changePost = post
    }
    
    func post(id: Int) -> Post? {
        let post = data.filter { $0.id == id }
        return post.first
    }
    
    func deletePosts(for user: User) {
        data = data.filter { $0.userId !=  user.userID }
    }
}

