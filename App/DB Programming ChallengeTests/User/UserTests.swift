//
//  UserTests.swift
//  DB Programming ChallengeTests
//
//  Created by Jan Olbrich on 13.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

import Quick
import Nimble

@testable import DB_Programming_Challenge

class UserTests: QuickSpec {
    override func spec() {
        describe("The user") {
            context("after initialization with an id") {
                it("sets its id correctly") {
                    let userID = 123456789
                    let sut: User = UserImpl(userID: userID)
                    
                    expect(sut.userID).to(equal(userID))
                }
            }
        }
    }
}
