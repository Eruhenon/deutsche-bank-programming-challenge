//
//  NSPersistentContainer+Store.swift
//  DB Programming ChallengeTests
//
//  Created by Jan Olbrich on 14.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

import CoreData

extension NSPersistentContainer {
    func destroyPersistentStore() {
        guard let storeURL = persistentStoreDescriptions.first?.url,
            let storeType = persistentStoreDescriptions.first?.type else {
                return
        }
        
        do {
            let persistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: NSManagedObjectModel())
            try persistentStoreCoordinator.destroyPersistentStore(at: storeURL, ofType: storeType, options: nil)
        } catch let error {
            print("print to destroy persistent store at \(storeURL), error: \(error)")
        }
    }
}
