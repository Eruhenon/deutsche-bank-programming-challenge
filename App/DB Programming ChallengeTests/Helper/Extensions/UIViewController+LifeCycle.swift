//
//  UIViewController+LifeCycle.swift
//  DB Programming ChallengeTests
//
//  Created by Jan Olbrich on 13.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

import UIKit

extension UIViewController {
    func initializeLifeCycle() {
        _ = self.view
    }
}
