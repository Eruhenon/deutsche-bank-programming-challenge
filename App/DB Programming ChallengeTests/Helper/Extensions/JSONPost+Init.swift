//
//  JSONPost+Init.swift
//  DB Programming ChallengeTests
//
//  Created by Jan Olbrich on 14.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

@testable import DB_Programming_Challenge

extension JSONPost {
    init(testID: Int = -1, testUserId: Int = -1, testTitle: String = "", testBody: String = "") {
        self.init(id: testID, userId: testUserId, title: testTitle, body: testBody)
    }
}
