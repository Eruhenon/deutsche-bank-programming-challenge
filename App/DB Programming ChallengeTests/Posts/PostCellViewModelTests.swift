//
//  PostCellViewModelTests.swift
//  DB Programming ChallengeTests
//
//  Created by Jan Olbrich on 14.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

import Quick
import Nimble

@testable import DB_Programming_Challenge

class PostCellViewModelTests: QuickSpec {
    override func spec() {
        describe("The PostCellViewModel") {
            var sut: PostCellViewModel!
            
            beforeEach {
                let post = PostImpl(id: 1, userId: 1, title: "testTitle", body: "testBody", favorite: false)
                sut = PostCellViewModelImpl(post: post)
            }
            
            afterEach {
                sut = nil
            }
            
            context("initialized with a Post") {
                it("has the correct title") {
                    expect(sut.title).to(equal("testTitle"))
                }
                
                it("has the correct body") {
                    expect(sut.body).to(equal("testBody"))
                }
            }
            
            context("calling toggleFavorite") {
                it("changes toggles its favorite state") {
                    let oldState = sut.favorite
                    
                    sut.toggleFavorite()
                    
                    expect(sut.favorite).to(equal(!oldState))
                }
                
                it("informs its cellDelegate about dataChanges") {
                    let mock = PostCellViewModelDelegateMock()
                    sut.cellDelegate = mock
                    
                    sut.toggleFavorite()
                    
                    expect(mock.didCallNeedsUpdate).to(beTrue())
                }
                
                it("informs its viewModelDelegate to toggleFavorite") {
                    let mock = PostCellViewModelVMDelegateMock()
                    sut.vmDelegate = mock
                    
                    sut.toggleFavorite()
                    
                    expect(mock.didCallToggleFavorite).to(beTrue())
                }
                
                it("passes the correct postId to the viewModelDelegate") {
                    let mock = PostCellViewModelVMDelegateMock()
                    sut.vmDelegate = mock
                    
                    sut.toggleFavorite()
                    
                    expect(mock.passedPostId).to(equal(1))
                }
            }
        }
    }
}
