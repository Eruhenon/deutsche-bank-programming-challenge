//
//  PostsViewModelTests.swift
//  DB Programming ChallengeTests
//
//  Created by Jan Olbrich on 13.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

import Quick
import Nimble

@testable import DB_Programming_Challenge

class PostsViewModelTests: QuickSpec {
    override func spec() {
        describe("The PostsViewModel") {
            context("after initialization") {
                it("returns 1 section") {
                    let sut = PostsViewModelImpl(user: UserImpl(userID: 123), storage: PostsStorageMock())
                    expect(sut.numberOfSections()).to(equal(1))
                }
                
                it("provides the correct segmentedControl titles") {
                    let sut = PostsViewModelImpl(user: UserImpl(userID: 123), storage: PostsStorageMock())
                    
                    expect(sut.segmentedControlTitles[0]).to(equal("All"))
                    expect(sut.segmentedControlTitles[1]).to(equal("Favourites"))
                }
                
                it("requests to update the data via the storage") {
                    let mock = PostsStorageMock()
                    _ = PostsViewModelImpl(user: UserImpl(userID: 123), storage: mock)
                    
                    expect(mock.didCallUpdateForUser).to(beTrue())
                }
                
                it("loads the local data") {
                    let mock = PostsStorageMock()
                    let sut = PostsViewModelImpl(user: UserImpl(userID: 123), storage: mock)
                    
                    expect(sut.numberOfRows(section: 0)).to(equal(1))
                }
                
                it("sets itself as the storage's delegate") {
                    let mock = PostsStorageMock()
                    let sut = PostsViewModelImpl(user: UserImpl(userID: 123), storage: mock)
                    
                    expect(mock.delegate).to(be(sut))
                }
            }
            
            context("requesting a cellViewModel") {
                it("returns nil if the index is out of bounds") {
                    let mock = PostsStorageMock()
                    let sut = PostsViewModelImpl(user: UserImpl(userID: 123), storage: mock)
                    
                    expect(sut.cellViewModel(for: IndexPath(row: 100, section: 1))).to(beNil())
                }
                
                it("returns the correct CellViewModel") {
                    let mock = PostsStorageMock()
                    let sut = PostsViewModelImpl(user: UserImpl(userID: 123), storage: mock)
                    
                    expect(sut.cellViewModel(for: IndexPath(row: 0, section: 1))?.body).to(equal("TestMockBody"))
                }
            }
            
            context("calling postStorage(didUpdate:)") {
                it("will update its itemcount") {
                    let mock = PostsStorageMock()
                    let sut = PostsViewModelImpl(user: UserImpl(userID: 123), storage: mock)
                    sut.postsStorage(mock, didUpdate: [PostImpl()])
                    
                    expect(sut.numberOfRows(section: 0)).to(equal(1))
                }
                
                it("will inform its delegate") {
                    let mock = PostsStorageMock()
                    let delegateMock = PostsViewModelDelegateMock()
                    let sut = PostsViewModelImpl(user: UserImpl(userID: 123), storage: mock)
                    sut.delegate = delegateMock
                    sut.postsStorage(mock, didUpdate: [PostImpl(), PostImpl(), PostImpl()])
                    
                    expect(delegateMock.didCallDidUpdate).toEventually(beTrue())
                }
            }
            
            context("did receive toggleRequest") {
                it("requests to change the favorite state accordingly") {
                    let mock = PostsStorageMock()
                    let sut = PostsViewModelImpl(user: UserImpl(userID: 123), storage: mock)
                    
                    sut.postCellViewModel(PostCellViewModelImpl(post: PostImpl()), toggleFavorite: 100) 
                    
                    expect(mock.didRequestFavoriteChange).to(beTrue())
                }
            }
            
            context("requesting filtered Data") {
                it("returns the correct filtered row count") {
                    let mock = PostsStorageMock()
                    mock.posts = [PostImpl(testID: 1), PostImpl(testID: 2), PostImpl(testID: 3), PostImpl(testID: 4, testLike: true), PostImpl(testID: 5, testLike: true)]
                    let sut = PostsViewModelImpl(user: UserImpl(userID: 123), storage: mock)
                    
                    expect(sut.numberOfFavoriteRows(section: 1)).to(equal(2))
                }
                
                it("returns the correct cellViewModel row count") {
                    let mock = PostsStorageMock()
                    mock.posts = [PostImpl(testID: 1), PostImpl(testID: 2), PostImpl(testID: 3), PostImpl(testID: 4, testLike: true), PostImpl(testID: 5, testLike: true)]
                    let sut = PostsViewModelImpl(user: UserImpl(userID: 123), storage: mock)
                    
                    let count = sut.numberOfFavoriteRows(section: 1)
                    for row in 0..<count {
                        let vm = sut.cellViewModelFavorite(for: IndexPath(row: row, section: 1))
                        
                        expect(vm?.favorite).to(beTrue())
                    }
                }
            }
        }
    }
}
