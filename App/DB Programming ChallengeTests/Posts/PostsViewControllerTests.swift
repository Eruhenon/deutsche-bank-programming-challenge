//
//  PostsViewControllerTests.swift
//  DB Programming ChallengeTests
//
//  Created by Jan Olbrich on 13.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

import Quick
import Nimble

@testable import DB_Programming_Challenge

class PostsViewControllerTests: QuickSpec {
    override func spec() {
        describe("The PostsViewController") {
            context("After Initialization") {
                it("is not nil") {
                    let sut = StoryboardScene.ViewPosts.showPostsScene.instantiate()
                    sut.viewModel = PostsViewModelMock()
                    expect(sut).notTo(beNil())
                }
                
                it("has a segmented Control after viewDidLoad") {
                    let sut = StoryboardScene.ViewPosts.showPostsScene.instantiate()
                    sut.viewModel = PostsViewModelMock()
                    sut.initializeLifeCycle()
                    sut.viewWillAppear(false)
                    
                    expect(sut.tableView.tableHeaderView).to(beAKindOf(UISegmentedControl.self))
                }
                
                it("has an action connected to the segmented Control") {
                    let sut = StoryboardScene.ViewPosts.showPostsScene.instantiate()
                    sut.viewModel = PostsViewModelMock()
                    sut.initializeLifeCycle()
                    sut.viewWillAppear(false)
                    
                    let segmentedControl = sut.tableView.tableHeaderView as! UISegmentedControl
                    
                    expect(segmentedControl.actions(forTarget: sut, forControlEvent: .valueChanged)?.count).to(beGreaterThan(0))
                }
                
                it("has index 0 selected by default") {
                    let sut = StoryboardScene.ViewPosts.showPostsScene.instantiate()
                    sut.viewModel = PostsViewModelMock()
                    sut.initializeLifeCycle()
                    sut.viewWillAppear(false)
                    
                    let segmentedControl = sut.tableView.tableHeaderView as! UISegmentedControl
                    
                    expect(segmentedControl.selectedSegmentIndex).to(equal(0))
                }
            }
            
            context("setting the ViewModel and calling viewWillAppear") {
                it("hat the correct titlees in the segmented control") {
                    let mock: PostsViewModel = PostsViewModelMock()
                    let sut = StoryboardScene.ViewPosts.showPostsScene.instantiate()
                    sut.viewModel = mock
                    sut.initializeLifeCycle()
                    
                    sut.viewWillAppear(false)
                    
                    let segmentedControl = sut.tableView.tableHeaderView as! UISegmentedControl
                    expect(segmentedControl.titleForSegment(at: 0)).to(equal(mock.segmentedControlTitles[0]))
                    expect(segmentedControl.titleForSegment(at: 1)).to(equal(mock.segmentedControlTitles[1]))
                }
            }
            
            context("the TableViewDelegateMethods") {
                it("returns the correct number of Sections") {
                    let mock: PostsViewModel = PostsViewModelMock()
                    let sut = StoryboardScene.ViewPosts.showPostsScene.instantiate()
                    sut.viewModel = mock
                    
                    expect(sut.numberOfSections(in: sut.tableView)).to(equal(mock.numberOfSections()))
                }
                
                it("returns the correct number of cells for section 0") {
                    let mock: PostsViewModel = PostsViewModelMock()
                    let sut = StoryboardScene.ViewPosts.showPostsScene.instantiate()
                    sut.viewModel = mock
                    sut.initializeLifeCycle()
                    sut.viewWillAppear(false)
                    
                    sut.segmentedControl?.selectedSegmentIndex = 0
                    
                    expect(sut.tableView(sut.tableView, numberOfRowsInSection: 0 )).to(equal(mock.numberOfRows(section: 0)))
                }
                
                it("returns the correct number of filtered cells for section 0") {
                    let mock: PostsViewModel = PostsViewModelMock()
                    let sut = StoryboardScene.ViewPosts.showPostsScene.instantiate()
                    sut.viewModel = mock
                    sut.initializeLifeCycle()
                    sut.viewWillAppear(false)
                    
                    sut.segmentedControl?.selectedSegmentIndex = 1
                    
                    expect(sut.tableView(sut.tableView, numberOfRowsInSection: 0 )).to(equal(mock.numberOfFavoriteRows(section: 0)))
                }
                
                it("returns a cell of the correct type") {
                    let mock: PostsViewModel = PostsViewModelMock()
                    let sut = StoryboardScene.ViewPosts.showPostsScene.instantiate()
                    sut.viewModel = mock
                    
                    let cell = sut.tableView(sut.tableView, cellForRowAt: IndexPath(row: 0, section: 0))
                    expect(cell).to(beAKindOf(PostTableViewCell.self))
                }
                
                it("configures cell correctly") {
                    let mock: PostsViewModel = PostsViewModelMock()
                    let sut = StoryboardScene.ViewPosts.showPostsScene.instantiate()
                    sut.viewModel = mock
                    sut.initializeLifeCycle()
                    sut.viewWillAppear(false)
                    
                    let cell = sut.tableView(sut.tableView, cellForRowAt: IndexPath(row: 0, section: 0)) as! PostTableViewCell
                    expect(cell.titleLabel.text).to(equal("TestTitle"))
                    expect(cell.bodyLabel.text).to(equal("TestBody"))
                }
                
                
                it("returns filtered cells correctly") {
                    let mock = PostsStorageMock()
                    mock.posts = [PostImpl(testID: 1), PostImpl(testID: 2), PostImpl(testID: 3), PostImpl(testID: 4, testLike: true), PostImpl(testID: 5, testLike: true)]
                    let viewModel = PostsViewModelImpl(user: UserImpl(userID: 123), storage: mock)
                    let sut = StoryboardScene.ViewPosts.showPostsScene.instantiate()
                    sut.viewModel = viewModel
                    sut.initializeLifeCycle()
                    sut.viewWillAppear(false)
                    
                    sut.segmentedControl?.selectedSegmentIndex = 1
                    
                    let count = sut.tableView(sut.tableView, numberOfRowsInSection: 1)
                    for row in 0..<count {
                        let cell = sut.tableView(sut.tableView, cellForRowAt: IndexPath(row: row, section: 1)) as! PostTableViewCell
                        
                        expect(cell.viewModel?.favorite).to(beTrue())
                    }
                }
                
            }
            
            context("receiving a ViewModelDelegate message") {
                it("reloads the tableView") {
                    let mock = UITableViewMock()
                    let sut = StoryboardScene.ViewPosts.showPostsScene.instantiate()
                    sut.viewModel = PostsViewModelMock()
                    sut.tableView = mock
                    sut.initializeLifeCycle()
                    
                    sut.postsViewModel(sut.viewModel, didUpdate: [])
                    
                    expect(mock.shouldReloadData).to(beTrue())
                }
            }
            
            context("changing the value of the selected control to favorite") {
                it("reloads the tableView") {
                    let mock = UITableViewMock()
                    let sut = StoryboardScene.ViewPosts.showPostsScene.instantiate()
                    sut.viewModel = PostsViewModelMock()
                    sut.tableView = mock
                    sut.initializeLifeCycle()
                    sut.viewWillAppear(false)
                    
                    let segmentedControl = sut.tableView.tableHeaderView as! UISegmentedControl
                    segmentedControl.sendActions(for: .valueChanged)
                    
                    expect(mock.shouldReloadData).to(beTrue())
                }
            }
        }
    }
}
