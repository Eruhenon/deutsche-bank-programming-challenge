//
//  PostCellViewModelDelegateMock.swift
//  DB Programming ChallengeTests
//
//  Created by Jan Olbrich on 15.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

@testable import DB_Programming_Challenge

class PostCellViewModelDelegateMock: PostCellViewModelDelegate {
    var didCallNeedsUpdate = false
    
    func postCellViewModel(_ viewModel: PostCellViewModel, needsUpdate: Bool) {
        didCallNeedsUpdate = true
    }
}
