//
//  UITableViewMock.swift
//  DB Programming ChallengeTests
//
//  Created by Jan Olbrich on 14.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

import UIKit

class UITableViewMock: UITableView {
    var shouldReloadData = false
    
    override func reloadData() {
        shouldReloadData = true
        super.reloadData()
    }
}
