//
//  PostsStorageMock.swift
//  DB Programming ChallengeTests
//
//  Created by Jan Olbrich on 13.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

@testable import DB_Programming_Challenge

class PostsStorageMock: PostsStorage {
    var didCallUpdateForUser = false
    var didRequestFavoriteChange = false
    var delegate: PostsStorageDelegate?
    var posts = [PostImpl(testID: 100, testBody:"TestMockBody")]
    
    func update(for: User) {
        didCallUpdateForUser = true
    }
    
    func posts(for: User) -> [Post] {
        return posts
    }
    
    func toggleFavorite(post: Int) {
        didRequestFavoriteChange = true
    }
}
