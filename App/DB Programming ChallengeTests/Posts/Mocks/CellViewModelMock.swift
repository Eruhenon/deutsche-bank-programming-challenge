//
//  CellViewModelMock.swift
//  DB Programming ChallengeTests
//
//  Created by Jan Olbrich on 15.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

@testable import DB_Programming_Challenge

class CellViewModelMock: PostCellViewModel {
    var title = "DemoTitle"
    var body = "DemoBody"
    var favorite = false
    var cellDelegate: PostCellViewModelDelegate?
    var vmDelegate: PostCellViewModelVMDelegate?
    
    var didCallToggleFavorite = false
    
    func toggleFavorite() {
        didCallToggleFavorite = true
    }
}
