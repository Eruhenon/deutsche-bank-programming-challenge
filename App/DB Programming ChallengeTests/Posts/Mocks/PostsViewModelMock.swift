//
//  PostsViewModelMock.swift
//  DB Programming ChallengeTests
//
//  Created by Jan Olbrich on 13.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

import Foundation

@testable import DB_Programming_Challenge

class PostsViewModelMock: PostsViewModel {
    var didRequestCellViewModel = false
    
    var delegate: PostsViewModelDelegate?
    let segmentedControlTitles = ["Left", "Right"]
    
    func numberOfSections() -> Int {
        return 3
    }
    
    func numberOfRows(section: Int) -> Int {
        switch section {
            case 0: return 2
            case 1: return 4
            case 2: return 5
            default: return 0
        }
    }
    
    func numberOfFavoriteRows(section: Int) -> Int {
        return 7
    }
    
    func cellViewModel(for: IndexPath) -> PostCellViewModel? {
        didRequestCellViewModel = true
        
        let post = PostImpl(id: 1, userId: 1, title: "TestTitle", body: "TestBody", favorite: false)
        return PostCellViewModelImpl(post: post)
    }
    
    func cellViewModelFavorite(for: IndexPath) -> PostCellViewModel? {
        return nil
    }
}
