//
//  PostsViewModelDelegateMock.swift
//  DB Programming ChallengeTests
//
//  Created by Jan Olbrich on 13.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

@testable import DB_Programming_Challenge

class PostsViewModelDelegateMock: PostsViewModelDelegate {
    var didCallDidUpdate = false
    
    func postsViewModel(_ viewModel: PostsViewModel, didUpdate: [PostCellViewModel]) {
        didCallDidUpdate = true
    }
}
