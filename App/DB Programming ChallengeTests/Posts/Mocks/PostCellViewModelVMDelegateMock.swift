//
//  PostCellViewModelVMDelegateMock.swift
//  DB Programming ChallengeTests
//
//  Created by Jan Olbrich on 15.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

@testable import DB_Programming_Challenge

class PostCellViewModelVMDelegateMock: PostCellViewModelVMDelegate {
    var didCallToggleFavorite = false
    var passedPostId = -1
    
    func postCellViewModel(_ viewModel: PostCellViewModel, toggleFavorite: Int) {
        didCallToggleFavorite = true
        passedPostId = toggleFavorite
    }
}
