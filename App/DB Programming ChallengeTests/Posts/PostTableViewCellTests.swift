//
//  PostTableViewCellTests.swift
//  DB Programming ChallengeTests
//
//  Created by Jan Olbrich on 14.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

import Quick
import Nimble

@testable import DB_Programming_Challenge

class PostTableViewCellTests: QuickSpec {
    override func spec() {
        describe("The PostTableViewCell") {
            var sut: PostTableViewCell!
            
            beforeEach {
                let allViewsInXibArray = Bundle.main.loadNibNamed("PostTableViewCell", owner: self, options: nil)
                sut = (allViewsInXibArray?.first as! PostTableViewCell)
            }
            afterEach {
                sut = nil
            }
            
            context("after initialization") {
                it("has a titleLabel connected") {
                    expect(sut.titleLabel).notTo(beNil())
                }
                
                it("has a bodyLabel connected") {
                    expect(sut.bodyLabel).notTo(beNil())
                }
                
                it("has a favoriteButton") {
                    expect(sut.favoriteButton).notTo(beNil())
                }
                
                it("has the like button with an TouchUpInside action") {
                    expect(sut.favoriteButton.actions(forTarget: sut, forControlEvent: .touchUpInside)?.count).to(beGreaterThan(0))
                }
            }
            
            context("setting a viewModel") {
                beforeEach {
                    let post = PostImpl(id: 1, userId: 1, title: "testTitle", body: "DemoBody", favorite: false)
                    let vm = PostCellViewModelImpl(post: post)
                    sut.viewModel = vm
                }
                
                it("has the correct title set") {
                    expect(sut.titleLabel.text).to(equal("testTitle"))
                }
                
                it("has the correct body set") {
                    expect(sut.bodyLabel.text).to(equal("DemoBody"))
                }
            }
            
            context("pressing like") {
                it("calls the viewModel") {
                    let mock = CellViewModelMock()
                    sut.viewModel = mock
                    
                    sut.favoriteButton.sendActions(for: .touchUpInside)
                    
                    expect(mock.didCallToggleFavorite).to(beTrue())
                }
            }
            
            context("receiving a data update request") {
                it("updates the ui") {
                    let mock = CellViewModelMock()
                    sut.viewModel = mock
                    mock.title = "Help me"
                    
                    sut.postCellViewModel(mock, needsUpdate: true)
                    
                    expect(sut.titleLabel.text).to(equal(mock.title))
                }
            }
        }
    }
}
