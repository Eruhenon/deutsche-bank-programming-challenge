//
//  LoginIntegrationTests.swift
//  DB Programming Challenge IntegrationTests
//
//  Created by Jan Olbrich on 14.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

import Quick
import Nimble

@testable import DB_Programming_Challenge

class LoginIntegrationTests: QuickSpec {
    override func spec() {
        describe("Integration of the LoginViewController") {
            context("trying to login with a valid user") {
                var navigationController: UINavigationController!
                var sut: LoginViewController!
                
                beforeEach {
                    let appDelegate = AppDelegate()
                    _ = appDelegate.application(UIApplication.shared, didFinishLaunchingWithOptions: nil)
                    
                    navigationController = (appDelegate.window!.rootViewController as! UINavigationController)
                    sut = (navigationController.topViewController as! LoginViewController)
                    sut.initializeLifeCycle()
                    
                    sut.userIDTextField.text = "123"
                }
                
                afterEach {
                    navigationController = nil
                    sut = nil
                }
                
                it("navigates to the PostsViewController") {
                    sut.login()
                    expect(navigationController.topViewController).toEventually(beAKindOf(PostsViewController.self))
                    
                    let postsViewController = navigationController.topViewController as! PostsViewController
                    expect(postsViewController.viewModel).notTo(beNil())
                    expect(postsViewController.viewModel.delegate).to(be(postsViewController))
                }
            }
        }
    }
}
