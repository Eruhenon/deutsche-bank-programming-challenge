//
//  StoryboardStageVisitorImpl.swift
//  DB Programming Challenge
//
//  Created by Jan Olbrich on 13.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

import UIKit

class StoryboardStageVisitorImpl: StoryboardStageVisitor {
    let httpClient: HTTPClient
    let coreDataManager: CoreDataManager
    
    init(httpClient: HTTPClient, coreData: CoreDataManager) {
        self.httpClient = httpClient
        self.coreDataManager = coreData
    }
    
    func prepare(for segue: UIStoryboardSegue, sender: Any?, context: StoryboardStageData?) {
        guard let viewController = segue.destination as? StoryboardStage else {
            print("Segue destination is not supporting the StoryboardStage protocol")
            return
        }
        
        viewController.accept(visitor: self, context: context)
    }
}
