//
//  StoryboardStage.swift
//  DB Programming Challenge
//
//  Created by Jan Olbrich on 13.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

protocol StoryboardStage {
    func accept(visitor: StoryboardStageVisitor, context: StoryboardStageData?)
}
