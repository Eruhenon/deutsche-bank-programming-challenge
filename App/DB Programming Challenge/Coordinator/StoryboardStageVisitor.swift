//
//  StoryboardStageVisitor.swift
//  DB Programming Challenge
//
//  Created by Jan Olbrich on 13.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

import UIKit

protocol StoryboardStageVisitor {
    var coreDataManager: CoreDataManager { get }
    var httpClient: HTTPClient { get }
    
    func prepare(for segue: UIStoryboardSegue, sender: Any?, context: StoryboardStageData?)
}
