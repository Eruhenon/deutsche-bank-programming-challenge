//
//  AppDelegate.swift
//  DB Programming Challenge
//
//  Created by Jan Olbrich on 13.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

import CoreData
import UIKit

class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let initialVC = StoryboardScene.ViewPosts.initialScene.instantiate()
        
        let httpClient = HTTPClientImpl()
        let coreDataManager = CoreDataManagerImpl()
        coreDataManager.setup(type: NSSQLiteStoreType) {
            print("successfully created CoreData Stack")
        }
        
        let coordinator = StoryboardStageVisitorImpl(httpClient: httpClient, coreData: coreDataManager)
        let loginViewController = initialVC.topViewController as! LoginViewController // we can force unwrap as otherwise the app is broken anyways
        loginViewController.viewModel = LoginViewModelImpl()
        loginViewController.coordinator = coordinator
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = initialVC
        window?.makeKeyAndVisible()
        return true
    }
}

