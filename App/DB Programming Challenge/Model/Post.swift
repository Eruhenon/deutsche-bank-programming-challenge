//
//  Post.swift
//  DB Programming Challenge
//
//  Created by Jan Olbrich on 13.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

protocol Post {
    var id: Int { get }
    var userId: Int { get }
    var title: String { get }
    var body: String { get }
    var favorite: Bool { get }
}
