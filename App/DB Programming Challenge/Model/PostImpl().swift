//
//  PostImpl().swift
//  DB Programming Challenge
//
//  Created by Jan Olbrich on 13.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

struct PostImpl: Post {
    let id: Int
    let userId: Int
    let title: String
    let body: String
    let favorite: Bool
}
