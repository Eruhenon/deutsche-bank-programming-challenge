//
//  HTTPClientImpl.swift
//  DB Programming Challenge
//
//  Created by Jan Olbrich on 13.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

import Foundation

class HTTPClientImpl: HTTPClient {
    private func httpRequest(request: URLRequest, callback: @escaping (Data?, Error?) -> Void) {
        let session = URLSession.shared
        let _ = session.dataTask(with: request) { data, response, error in
            if let error = error {
                callback(nil, error)
                return
            }
            
            callback(data, nil)
        }.resume()
    }
    
    private func httpGet(request: URL, callback: @escaping (Data?, Error?) -> Void) {
        let urlRequest = URLRequest(url: request)
        httpRequest(request: urlRequest, callback: callback)
    }
    
    func decodedJsonGet<T: Decodable>(url: URL, type: T.Type, completion: @escaping (T?, Error?)->Void) {
        httpGet(request: url) { (data, error) in
            if let error = error {
                completion(nil, error)
                return
            }
            
            if let data = data {
                do {
                    let result = try JSONDecoder().decode(T.self, from: data)
                    completion(result, nil)
                } catch let jsonError {
                    completion(nil, jsonError)
                }
            }
            else {
                let error = NSError(domain:"HTTPClient", code:200, userInfo:nil)
                completion(nil, error)
            }
        }
    }
}
