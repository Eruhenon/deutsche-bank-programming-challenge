//
//  HTTPClient.swift
//  DB Programming Challenge
//
//  Created by Jan Olbrich on 13.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

import Foundation

protocol HTTPClient: AnyObject {
    func decodedJsonGet<T: Decodable>(url: URL, type: T.Type, completion: @escaping (T?, Error?)->Void)
}
