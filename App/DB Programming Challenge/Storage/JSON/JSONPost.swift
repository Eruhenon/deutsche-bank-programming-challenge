//
//  JSONPost.swift
//  DB Programming Challenge
//
//  Created by Jan Olbrich on 13.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

import Foundation

struct JSONPost: Decodable {
    let id: Int
    let userId: Int
    let title: String
    let body: String
}
