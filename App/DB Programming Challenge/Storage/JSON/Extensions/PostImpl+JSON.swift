//
//  PostImpl+JSON.swift
//  DB Programming Challenge
//
//  Created by Jan Olbrich on 13.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

extension PostImpl {
    init(json: JSONPost) {
        id = json.id
        userId = json.userId
        title = json.title
        body = json.body
        favorite = false
    }
}
