//
//  PostsStorage.swift
//  DB Programming Challenge
//
//  Created by Jan Olbrich on 13.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

protocol PostsStorage: AnyObject {
    var delegate: PostsStorageDelegate? { get set }
    
    func update(for: User)
    func posts(for: User) -> [Post]
    func toggleFavorite(post: Int)
}
