//
//  PostsStorageImpl.swift
//  DB Programming Challenge
//
//  Created by Jan Olbrich on 13.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

import Foundation

class PostsStorageImpl: PostsStorage {
    private var httpClient: HTTPClient
    private var database: Database
    
    weak var delegate: PostsStorageDelegate?
    
    init(httpClient: HTTPClient, database: Database) {
        self.httpClient = httpClient
        self.database = database
    }
    
    func update(for user: User) {
        guard let targetURL = URL(string: "https://jsonplaceholder.typicode.com/posts?userId=\(user.userID)") else {
            return
        }
        
        httpClient.decodedJsonGet(url: targetURL, type: [JSONPost].self) { jsonPosts, error in
            if let error = error {
                self.delegate?.postsStorage(self, didFailToUpdate: error)
                return
            }
            
            if let jsonPosts = jsonPosts {
                let posts = jsonPosts.map { PostImpl(json: $0) }
                
                let oldPostsIds = self.database.posts(for: user).filter {
                    $0.favorite == true
                }.map {
                    return $0.id
                }
                
                let preservedFavoriteState = posts.map { post -> Post in
                    if oldPostsIds.contains(post.id) {
                        return PostImpl(post: post, newFavorite: true)
                    }
                    
                    return post
                }
                
                self.database.deletePosts(for: user)
                self.database.update(posts: preservedFavoriteState)
                self.delegate?.postsStorage(self, didUpdate: posts)
            }
        }
    }
    
    func posts(for user: User) -> [Post] {
        return database.posts(for: user).sorted { $0.id < $1.id }
    }
    
    func toggleFavorite(post id: Int) {
        guard let oldPost = database.post(id: id) else {
            fatalError("PostID did not exist in database")
        }
        
        let post = PostImpl(post: oldPost, newFavorite: !oldPost.favorite)
        
        database.update(post: post)
        delegate?.postsStorage(self, didUpdate: [post])
    }
}
