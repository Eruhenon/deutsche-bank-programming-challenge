//
//  PostImpl+Change.swift
//  DB Programming Challenge
//
//  Created by Jan Olbrich on 15.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

extension PostImpl {
    init(post: Post, newFavorite: Bool) {
        id = post.id
        userId = post.userId
        title = post.title
        body = post.body
        favorite = newFavorite
    }
}
