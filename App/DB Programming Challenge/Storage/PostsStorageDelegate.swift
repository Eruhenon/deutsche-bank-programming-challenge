//
//  PostsStorageDelegate.swift
//  DB Programming Challenge
//
//  Created by Jan Olbrich on 13.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

protocol PostsStorageDelegate: AnyObject {
    func postsStorage(_ storage: PostsStorage, didUpdate: [Post])
    func postsStorage(_ storage: PostsStorage, didFailToUpdate: Error)
}
