//
//  main.swift
//  DB Programming Challenge
//
//  Created by Jan Olbrich on 13.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

import UIKit

    let appDelegateClass: AnyClass = NSClassFromString("TestingAppDelegate") ?? AppDelegate.self

    UIApplicationMain(CommandLine.argc, CommandLine.unsafeArgv, nil, NSStringFromClass(appDelegateClass)
)
