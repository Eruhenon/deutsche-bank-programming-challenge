//
//  UserImpl.swift
//  DB Programming Challenge
//
//  Created by Jan Olbrich on 13.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

class UserImpl: User {
    let userID: Int
    
    init(userID: Int) {
        self.userID = userID
    }
}
