//
//  LoginViewModelDelegate.swift
//  DB Programming Challenge
//
//  Created by Jan Olbrich on 13.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

protocol LoginViewModelDelegate: AnyObject {
    func loginViewModel(_: LoginViewModel, didLoginWith: User)
    func loginViewModel(_: LoginViewModel, loginDidFailWithError: Error)
}
