//
//  LoginViewModelImpl.swift
//  DB Programming Challenge
//
//  Created by Jan Olbrich on 13.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

import Foundation

class LoginViewModelImpl: LoginViewModel {
    private var user: User?
    
    let userIDLabelText = "UserID:"
    let loginButtonTitle = "Login"
    
    var segueData: StoryboardStageData? {
        return LoginStageData(user: user)
    }
    
    weak var delegate: LoginViewModelDelegate?
    
    func login(with userID: String) {
        guard let validUserID = Int(userID) else {
            let error = NSError(domain:"Login", code:100, userInfo:["userID": userID])
            delegate?.loginViewModel(self, loginDidFailWithError: error)
            return
        }
        
        user = UserImpl(userID: validUserID)
        delegate?.loginViewModel(self, didLoginWith: user!)
    }
}
