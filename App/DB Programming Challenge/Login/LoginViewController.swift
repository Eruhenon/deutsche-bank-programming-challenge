//
//  LoginViewController.swift
//  DB Programming Challenge
//
//  Created by Jan Olbrich on 13.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    @IBOutlet weak var userIDLabel: UILabel!
    @IBOutlet weak var userIDTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    
    var viewModel: LoginViewModel! {
        didSet {
            if let vm = viewModel {
                vm.delegate = self
            }
        }
    }
    var coordinator: StoryboardStageVisitor!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        assertProperties()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        userIDLabel.text = viewModel.userIDLabelText
        loginButton.setTitle(viewModel.loginButtonTitle, for: .normal)
    }
    
    @IBAction func login() {
        let id = userIDTextField.text ?? ""
        viewModel.login(with: id)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let data = viewModel.segueData
        coordinator.prepare(for: segue, sender: sender, context: data)
    }
}

extension LoginViewController {
    func assertProperties() {
        assert(viewModel != nil)
        assert(coordinator != nil)
    }
}

extension LoginViewController: LoginViewModelDelegate {
    func loginViewModel(_: LoginViewModel, didLoginWith: User) {
        perform(segue: StoryboardSegue.ViewPosts.showPosts)
    }
    
    func loginViewModel(_: LoginViewModel, loginDidFailWithError: Error) {
        
    }
}
