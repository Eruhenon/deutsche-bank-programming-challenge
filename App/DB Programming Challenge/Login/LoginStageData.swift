//
//  LoginStageData.swift
//  DB Programming Challenge
//
//  Created by Jan Olbrich on 14.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

struct LoginStageData: StoryboardStageData {
    let user: User
    
    init?(user: User?) {
        guard let user = user else {
            return nil
        }
        self.user = user
    }
}
