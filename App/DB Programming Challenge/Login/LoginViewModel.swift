//
//  LoginViewModel.swift
//  DB Programming Challenge
//
//  Created by Jan Olbrich on 13.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

import Foundation

protocol LoginViewModel: AnyObject {
    var delegate: LoginViewModelDelegate? { get set }
    var userIDLabelText: String { get }
    var loginButtonTitle: String { get }
    var segueData: StoryboardStageData? { get }
    
    func login(with: String)
}
