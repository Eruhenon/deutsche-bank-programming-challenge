//
//  PostTableViewCell.swift
//  DB Programming Challenge
//
//  Created by Jan Olbrich on 13.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

import UIKit

class PostTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var favoriteButton: UIButton!
    
    var viewModel: PostCellViewModel? {
        didSet {
            if let vm = viewModel {
                vm.cellDelegate = self
                updateUI()
            }
        }
    }
    
    @IBAction func toggleFavorite(_ sender: UIButton) {
        viewModel?.toggleFavorite()
    }
    
    fileprivate func updateUI() {
        titleLabel.text = viewModel?.title ?? ""
        bodyLabel.text = viewModel?.body ?? ""
        
        let image = viewModel?.favorite ?? false ? Asset.heartFilled : Asset.heartOutline
        favoriteButton.setImage(UIImage(asset: image), for: .normal)
    }
}

// MARK: - PostCellViewModelDelegate
extension PostTableViewCell: PostCellViewModelDelegate {
    func postCellViewModel(_ viewModel: PostCellViewModel, needsUpdate: Bool) {
        updateUI()
    }
}
