//
//  PostCellViewModelVMDelegate.swift
//  DB Programming Challenge
//
//  Created by Jan Olbrich on 15.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

protocol PostCellViewModelVMDelegate: AnyObject {
    func postCellViewModel(_ viewModel: PostCellViewModel, toggleFavorite: Int)
}
