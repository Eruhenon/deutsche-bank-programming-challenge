//
//  PostCellViewModel.swift
//  DB Programming Challenge
//
//  Created by Jan Olbrich on 13.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

protocol PostCellViewModel: AnyObject {
    var title: String { get }
    var body: String { get }
    var favorite: Bool { get }
    
    var cellDelegate: PostCellViewModelDelegate? { get set }
    var vmDelegate: PostCellViewModelVMDelegate? { get set }
    
    func toggleFavorite()
}
