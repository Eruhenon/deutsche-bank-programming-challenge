//
//  PostCellViewModelImpl.swift
//  DB Programming Challenge
//
//  Created by Jan Olbrich on 13.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

class PostCellViewModelImpl: PostCellViewModel {
    let title: String
    let body: String
    var favorite: Bool
    
    weak var cellDelegate: PostCellViewModelDelegate?
    weak var vmDelegate: PostCellViewModelVMDelegate?
    
    private let postId: Int
    
    init(post: Post) {
        title = post.title
        body = post.body
        favorite = post.favorite
        postId = post.id
    }
    
    func toggleFavorite() {
        vmDelegate?.postCellViewModel(self, toggleFavorite: postId)
        
        // this is for user feedback so the user doesn't have to wait for the tableView to reload
        favorite = !favorite
        cellDelegate?.postCellViewModel(self, needsUpdate: true)
    }
}
