//
//  StoryboardStageVisitor+PostsViewController.swift
//  DB Programming Challenge
//
//  Created by Jan Olbrich on 14.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

import CoreData

extension StoryboardStageVisitor {
    func accept(visitor: PostsViewController, context: StoryboardStageData?) {
        guard let context = context as? LoginStageData else {
            fatalError("PostsViewController didn't receive required Data")
        }
        
        let database = CoreDataDatabase(manager: coreDataManager)
        let storage = PostsStorageImpl(httpClient: httpClient, database: database)
        let viewModel = PostsViewModelImpl(user: context.user, storage: storage)
        viewModel.delegate = visitor
        visitor.viewModel = viewModel
    }
}
