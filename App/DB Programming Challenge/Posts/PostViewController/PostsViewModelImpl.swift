//
//  PostsViewModelImpl.swift
//  DB Programming Challenge
//
//  Created by Jan Olbrich on 13.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

import Foundation

class PostsViewModelImpl: PostsViewModel {
    let segmentedControlTitles = ["All", "Favourites"]
    var delegate: PostsViewModelDelegate?
    
    private let user: User
    private let storage: PostsStorage
    private var postCellViewModels: [PostCellViewModel] = []
    
    init(user: User, storage: PostsStorage) {
        self.user = user
        self.storage = storage
        self.storage.delegate = self
        self.storage.update(for: user)
        
        postCellViewModels = self.storage.posts(for: self.user).map { PostCellViewModelImpl(post: $0) }
    }
    
    func numberOfSections() -> Int {
        return 1
    }
    
    func numberOfRows(section: Int) -> Int {
        return postCellViewModels.count
    }
    
    func numberOfFavoriteRows(section: Int) -> Int {
        return postCellViewModels.filter { $0.favorite }.count
    }
    
    func cellViewModel(for indexPath: IndexPath) -> PostCellViewModel? {
        guard indexPath.row < postCellViewModels.count else {
            return nil
        }
        
        let vm = postCellViewModels[indexPath.row]
        vm.vmDelegate = self
        
        return vm
    }
    
    func cellViewModelFavorite(for indexPath: IndexPath) -> PostCellViewModel? {
        let filteredViewModels = postCellViewModels.filter { $0.favorite }
        guard indexPath.row < filteredViewModels.count else {
            return nil
        }
        
        let vm = filteredViewModels[indexPath.row]
        vm.vmDelegate = self
        
        return vm
    }
}

// MARK: - PostsStorageDelegate
extension PostsViewModelImpl: PostsStorageDelegate {
    func postsStorage(_ storage: PostsStorage, didUpdate posts: [Post]) {
        postCellViewModels = self.storage.posts(for: self.user).map { PostCellViewModelImpl(post: $0) }
        
        DispatchQueue.main.async {
            self.delegate?.postsViewModel(self, didUpdate: self.postCellViewModels)
        }
    }
    
    func postsStorage(_ storage: PostsStorage, didFailToUpdate: Error) {
        print("Did Fail to update posts. Currently this error is not forwarded to the user, but could be via the delegate")
    }
}

// MARK: - PostCellViewModelVMDelegate
extension PostsViewModelImpl: PostCellViewModelVMDelegate {
    func postCellViewModel(_ viewModel: PostCellViewModel, toggleFavorite postID: Int) {
        storage.toggleFavorite(post: postID)
    }
}
