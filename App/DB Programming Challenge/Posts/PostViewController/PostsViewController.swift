//
//  PostsViewController.swift
//  DB Programming Challenge
//
//  Created by Jan Olbrich on 13.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

import UIKit

class PostsViewController: UITableViewController {
    
    var viewModel: PostsViewModel!
    var segmentedControl: UISegmentedControl?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        assertProperties()
        
        let nib = UINib(nibName: "PostTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "PostCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        segmentedControl = UISegmentedControl(items: viewModel.segmentedControlTitles)
        segmentedControl?.selectedSegmentIndex = 0
        segmentedControl?.addTarget(self, action: #selector(applyFilter), for: .valueChanged)
        tableView.tableHeaderView = segmentedControl
    }
    
    @objc func applyFilter() {
        tableView.reloadData()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if segmentedControl?.selectedSegmentIndex != 0 {
            return viewModel.numberOfFavoriteRows(section: 0)
        }
        
        return viewModel.numberOfRows(section: section)
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PostCell", for: indexPath) as! PostTableViewCell

        if segmentedControl?.selectedSegmentIndex != 0 {
            cell.viewModel = viewModel.cellViewModelFavorite(for: indexPath)
        }
        else {
            cell.viewModel = viewModel.cellViewModel(for: indexPath)
        }

        return cell
    }
}

// MARK: - Assertion
extension PostsViewController {
    func assertProperties() {
        assert(viewModel != nil)
    }
}

// MARK: - Coordinator
extension PostsViewController: StoryboardStage {
    func accept(visitor: StoryboardStageVisitor, context: StoryboardStageData?) {
        visitor.accept(visitor: self, context: context)
    }
}

// MARK: - PostsViewModelDelegate {
extension PostsViewController: PostsViewModelDelegate {
    func postsViewModel(_ viewModel: PostsViewModel, didUpdate: [PostCellViewModel]) {
        tableView.reloadData()
    }
}
