//
//  PostsViewModel.swift
//  DB Programming Challenge
//
//  Created by Jan Olbrich on 13.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

import Foundation

protocol PostsViewModel {
    var segmentedControlTitles: [String] { get }
    var delegate: PostsViewModelDelegate? { get set }
    
    func numberOfSections() -> Int
    func numberOfRows(section: Int) -> Int
    func numberOfFavoriteRows(section: Int) -> Int
    func cellViewModel(for: IndexPath) -> PostCellViewModel?
    func cellViewModelFavorite(for: IndexPath) -> PostCellViewModel?
}
