//
//  PostsViewModelDelegate.swift
//  DB Programming Challenge
//
//  Created by Jan Olbrich on 13.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

protocol PostsViewModelDelegate: AnyObject {
    func postsViewModel(_ viewModel: PostsViewModel, didUpdate: [PostCellViewModel])
}
