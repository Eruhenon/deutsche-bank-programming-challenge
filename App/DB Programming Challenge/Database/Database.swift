//
//  Database.swift
//  DB Programming Challenge
//
//  Created by Jan Olbrich on 13.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

protocol Database: AnyObject {
    func posts(for: User) -> [Post]
    func post(id: Int) -> Post?
    func update(posts: [Post])
    func update(post: Post)
    func deletePosts(for: User)
}
