//
//  PostImpl+CoreData.swift
//  DB Programming Challenge
//
//  Created by Jan Olbrich on 14.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

extension PostImpl {
    init(coreData: PlaceholderPost) {
        // This won't support 32 bit, so we can safely assume that Int is 64 bit
        id = Int(coreData.id)
        userId = Int(coreData.userId)
        title = coreData.title
        body = coreData.body
        favorite = coreData.favorite
    }
}
