//
//  CoreDataManagerImpl.swift
//  DB Programming Challenge
//
//  Created by Jan Olbrich on 14.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

import CoreData

class CoreDataManagerImpl: CoreDataManager {
    private var storeType = NSSQLiteStoreType
    
    lazy var persistentContainer: NSPersistentContainer = {
        let persistentContainer = NSPersistentContainer(name: "PlaceholderModel")
        let description = persistentContainer.persistentStoreDescriptions.first
        description?.type = storeType
        
        return persistentContainer
    }()
    
    lazy var mainContext: NSManagedObjectContext = {
        let context = persistentContainer.viewContext
        context.automaticallyMergesChangesFromParent = true
        return context
    }()
    
    var backgroundContext: NSManagedObjectContext {
        let context = persistentContainer.newBackgroundContext()
        context.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        return context
    }
    
    func setup(type: String = NSSQLiteStoreType, completion: @escaping ()->Void) {
        storeType = type
        
        loadPersistentStore {
            completion()
        }
    }
    
    private func loadPersistentStore(completion: @escaping ()->Void) {
        persistentContainer.loadPersistentStores { (description, error) in
            guard error == nil else {
                fatalError("unable to load store \(String(describing: error))")
            }
            
            completion()
        }
    }
}
