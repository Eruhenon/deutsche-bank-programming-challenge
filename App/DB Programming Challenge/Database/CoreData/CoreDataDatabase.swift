//
//  CoreDataDatabase.swift
//  DB Programming Challenge
//
//  Created by Jan Olbrich on 14.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

import CoreData

// I'm aware CoreData is not a database, but it represents the persistance layer in this case
class CoreDataDatabase: Database {
    private let coreDataManager: CoreDataManager
    
    init(manager: CoreDataManager) {
        coreDataManager = manager
    }
    
    func posts(for user: User) -> [Post] {
        let context = coreDataManager.mainContext
        let fetchRequest = fetchPosts(for: user)
        
        var posts: [PlaceholderPost]? = []
        do {
            posts = try context.fetch(fetchRequest)
        } catch {
            fatalError("Failed to fetch Posts: \(error)")
        }
        return (posts ?? []).map { PostImpl(coreData: $0) }
    }
    
    func update(posts: [Post]) {
        let context = coreDataManager.backgroundContext
        
        context.performAndWait {
            // we don't need the result
            _ = posts.map { post -> PlaceholderPost in
                let entity = NSEntityDescription.insertNewObject(forEntityName: PlaceholderPost.entityName(), into: context) as! PlaceholderPost
                
                entity.id = Int64(post.id)
                entity.userId = Int64(post.userId)
                entity.body = post.body
                entity.title = post.title
                entity.favorite = post.favorite
                
                return entity
            }
            
            do {
                try context.save()
                try coreDataManager.mainContext.save()
            } catch {
                print(error)
            }
        }
    }
    
    func update(post: Post) {
        let context = coreDataManager.backgroundContext
        let fetchRequest = fetchPost(id: post.id)
        
        var posts: [PlaceholderPost]? = []
        context.performAndWait {
            do {
                posts = try context.fetch(fetchRequest)
                
                guard let result = posts?.first else {
                    print("no entity found for Post: \(post)")
                    return
                }
                result.favorite = post.favorite
                
                try context.save()
                try coreDataManager.mainContext.save()
            } catch {
                fatalError("Failed to fetch Post: \(error)")
            }
        }
    }
    
    func post(id: Int) -> Post? {
        let context = coreDataManager.mainContext
        let fetchRequest = fetchPost(id: id)
        
        var posts: [PlaceholderPost]? = []
        context.performAndWait {
            do {
                posts = try context.fetch(fetchRequest)
            } catch {
                fatalError("Failed to fetch Post: \(error)")
            }
        }
        
        if let post = posts?.first {
            return PostImpl(coreData: post)
        }
        
        return nil
    }
    
    func deletePosts(for user: User) {
        let context = coreDataManager.backgroundContext
        let fetchRequest = fetchPosts(for: user)
        fetchRequest.includesPropertyValues = false
        
        context.performAndWait {
            do {
                // NSBatchDeleteRequest only works with SQLLiteStore not InMemory
                // To keep the unit testing value, this uses the old way to delete data
                let items: [PlaceholderPost] = try context.fetch(fetchRequest)
                for item in items {
                    context.delete(item)
                }
                
                try context.save()
                try coreDataManager.mainContext.save()
            } catch let error as NSError {
                fatalError("Failed to delete Posts: \(error)")
            }
        }
        
    }
    
    private func fetchPosts(for user: User) -> NSFetchRequest<PlaceholderPost> {
        let request: NSFetchRequest<PlaceholderPost> = PlaceholderPost.fetchRequest()
        request.predicate = NSPredicate(format: "userId == \(user.userID)")
        
        return request
    }
    
    private func fetchPost(id: Int) -> NSFetchRequest<PlaceholderPost> {
        let request: NSFetchRequest<PlaceholderPost> = PlaceholderPost.fetchRequest()
        request.predicate = NSPredicate(format: "id == \(id)")
        
        return request
    }
}
