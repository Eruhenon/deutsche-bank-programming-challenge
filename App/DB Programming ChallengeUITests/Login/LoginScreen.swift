//
//  LoginScreen.swift
//  DB Programming ChallengeUITests
//
//  Created by Jan Olbrich on 17.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

import XCTest

class LoginScreen: UITestScreen {
    let identifier = "Login Scene"
    let loginButtonLabel = "Login"
    let userIDTextLabel = "userIDTextField"
    
    var userIDTextField: XCUIElement { return app.textFields[userIDTextLabel] }
    var loginButton: XCUIElement { return app.buttons[loginButtonLabel] }
    
    func setUserIDTextField(userID: String) {
        setElementText(userIDTextField, userID)
    }
    
    func doLogin(userID: String) {
        setUserIDTextField(userID: userID)
        
        loginButton.tap()
    }
    
    func doSuccessfulLogin(userID: String) -> PostsScreen {
        doLogin(userID: userID)
        return PostsScreen(app)
    }

    
    func clearTextFields() {
        setUserIDTextField(userID: "")
    }
}
