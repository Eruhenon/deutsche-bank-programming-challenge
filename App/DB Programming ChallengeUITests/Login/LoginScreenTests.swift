//
//  LoginScreenTests.swift
//  DB Programming ChallengeUITests
//
//  Created by Jan Olbrich on 17.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

import XCTest

class LoginScreenUITests: XCTestCase {
    
    var app: XCUIApplication!
    var initialScreen: LoginScreen!
    
    override func setUp() {
        continueAfterFailure = false
        XCUIApplication().launch()
        
        app = XCUIApplication()
        initialScreen = LoginScreen(app)
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testExample() {
        let loginScreen = initialScreen!
        
        let postsScreen = loginScreen.doSuccessfulLogin(userID: "1")
        
        assertTextIsShown(text: postsScreen.identifier, screen: postsScreen)
    }
}
