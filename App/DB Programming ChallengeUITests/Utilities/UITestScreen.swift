//
//  UITestScreen.swift
//  DB Programming ChallengeUITests
//
//  Created by Jan Olbrich on 17.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

import XCTest
import Foundation

class UITestScreen {
    var app: XCUIApplication
    
    init(_ application: XCUIApplication) {
        app = application
    }
    
    func getTextOnScreen(labeltext: String) -> XCUIElement {
        return app.staticTexts[labeltext]
    }
    
    func setElementText(_ element: XCUIElement, _ string: String ) {
        if !element.textCanBeSet() {
            XCTFail("Element is not a compatible with setting text. See the extension")
        }
        
        element.tap()
        element.typeText(string)
    }
}
