//
//  XCUIElement+TextHelper.swift
//  DB Programming ChallengeUITests
//
//  Created by Jan Olbrich on 17.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

import XCTest

extension XCUIElement {
    func textCanBeSet() -> Bool {
        if ![.textField, .secureTextField].contains(elementType) {
            return false
        }
        return true
    }
}
