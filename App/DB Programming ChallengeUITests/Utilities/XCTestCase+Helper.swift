//
//  XCTestCase+Helper.swift
//  DB Programming ChallengeUITests
//
//  Created by Jan Olbrich on 17.09.19.
//  Copyright © 2019 Jan Olbrich. All rights reserved.
//

import XCTest

extension XCTestCase {
    func assertTextIsShown(text: String, screen: UITestScreen, file: StaticString = #file, line: UInt = #line) {
        XCTAssertEqual(screen.getTextOnScreen(labeltext: text).label, text, "did not find text: " + text, file: file, line: line)
    }
}
